"""Keyboard input module.

Methods:
    readchar: Reads a single character.
    readkey: Reads a key press.
"""

__author__ = "Martin Eyre (Slice of Pi Club)"

# Standard library imports
import sys
import termios
import tty


def readchar():
    """Reads a single character.

    Returns:
        Character pressed.

    Raises:
        KeyboardInterrupt
    """
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    if ch == '0x03':
        raise KeyboardInterrupt
    return ch


def readkey(getchar_fn=None):
    """Reads a key press.

    Returns:
        16 = Up arrow
        17 = Down arrow
        18 = Right arrow
        19 = Left arrow
    """
    getchar = getchar_fn or readchar
    c1 = getchar()
    if ord(c1) != 0x1b:
        return c1
    c2 = getchar()
    if ord(c2) != 0x5b:
        return c1
    c3 = getchar()
    return chr(0x10 + ord(c3) - 65)
