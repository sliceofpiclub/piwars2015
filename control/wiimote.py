"""Nintendo Wiimote and nunchuk functionality.

Requires cwiid library to be installed:
sudo apt-get install python-cwiid

Each number turns on different LEDs on the Wiimote
    ex) if wii.led = 1, then LED 1 is on
    2  = LED 2          3  = LED 3          4  = LED 4
    5  = LED 1, 3       6  = LED 2, 3       7  = LED 1,2,3
    8  = LED 4          9  = LED 1, 4       10 = LED 2,4
    11 = LED 1,2,4      12 = LED 3,4        13 = LED 1,3,4
    14 = LED 2,3,4      15 = LED 1,2,3,4
    It counts up in binary to 15
"""
#!/usr/bin/env python2

__author__ = "Claire Gurman"

# Standard library imports
import ConfigParser
import logging
import os
import sys
import time
import warnings

# Third party imports
import bluetooth
import cwiid

CONFIG_FILE = 'config.ini'
config_location = os.path.join(os.path.dirname(__file__), CONFIG_FILE)


class WiimotePi(cwiid.Wiimote):

    def __init__(self, mac_address=None):
        # Wiimote accelerometer
        # changes if mote is turned or flicked left or right
        self.acc_xflat = 120
        self.acc_x90cc = 95
        self.acc_x90c = 145
        # changes when mote is pointed or flicked up or down
        self.acc_yflat = 120
        self.acc_yup = 95
        self.acc_ydown = 145
        # Changes with the motes rotation, or when pulled back or flicked
        # up/down
        self.acc_zflat = 145
        self.acc_z90 = 120
        self.acc_zupsidedown = 95

        # Nunchuk stick
        self.n_stick_xmax = 225
        self.n_stick_xmid = 125
        self.n_stick_xmin = 25
        self.n_stick_ymax = 225
        self.n_stick_ymid = 125
        self.n_stick_ymin = 30
        # Nunchuk accelerometer
        self.nacc_x90l = 70
        self.nacc_x90r = 175
        self.nacc_y90up = 175
        self.nacc_y90down = 70

        if bluetooth.is_valid_address(mac_address):
            self.mac_address = mac_address
        else:
            warnings.warn(
                'Invalid Wiimote MAC address.')

        super(WiimotePi, self).__init__()
        # Add cwiid.RPT_ACC if accelerometer is needed.
        self.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_EXT

        # Allow time for Wiimote to settle
        time.sleep(2)

    def get_settings(self):
        self.get_config()
        logging.debug('Has section {0}? {1}'.format(
            self.mac_address, self.config.has_section(self.mac_address)))
        if self.config.has_section(self.mac_address):
            self.n_stick_xmax = self.config.getfloat(
                self.mac_address, 'nunchuk stick xmax')
            self.n_stick_xmid = self.config.getfloat(
                self.mac_address, 'nunchuck stick xmid')
            self.n_stick_xmin = self.config.getfloat(
                self.mac_address, 'nunchuck stick xmin')
            self.n_stick_ymax = self.config.getfloat(
                self.mac_address, 'nunchuk stick ymax')
            self.n_stick_ymid = self.config.getfloat(
                self.mac_address, 'nunchuck stick ymid')
            self.n_stick_ymin = self.config.getfloat(
                self.mac_address, 'nunchuck stick ymin')
        else:
            self.calibrate()

    def calibrate(self):
        self.led = 0
        print 'Center the nunchuk stick by releasing it, then press A.'
        while not (self.state['buttons'] & cwiid.BTN_A):
            # Wait for A to be pressed.
            pass

        self.n_stick_xmid = self.state['nunchuk']['stick'][cwiid.X]
        self.n_stick_ymid = self.state['nunchuk']['stick'][cwiid.Y]
        logging.debug('n_stick_xmid, n_stick_ymid: {0}, {1}'.format(
            self.n_stick_xmid, self.n_stick_ymid))
        self.led = 1
        time.sleep(1)

        print 'Push the nunchuk stick fully up, then press A.'
        while not (self.state['buttons'] & cwiid.BTN_A and self.state['nunchuk']['stick'][cwiid.Y] > self.n_stick_ymid):
            # Wait for A to be pressed.
            pass

        self.n_stick_ymax = self.state['nunchuk']['stick'][cwiid.Y]
        logging.debug('n_stick_ymax: {0}'.format(self.n_stick_ymax))
        self.led = 2
        time.sleep(1)

        print 'Push the nunchuk stick fully right, then press A.'
        while not (self.state['buttons'] & cwiid.BTN_A and self.state['nunchuk']['stick'][cwiid.X] > self.n_stick_xmid):
            # Wait for A to be pressed.
            pass

        self.n_stick_xmax = self.state['nunchuk']['stick'][cwiid.X]
        logging.debug('n_stick_xmax: {0}'.format(self.n_stick_xmax))
        self.led = 3
        time.sleep(1)

        print 'Push the nunchuk stick fully down, then press A.'
        while not (self.state['buttons'] & cwiid.BTN_A and self.state['nunchuk']['stick'][cwiid.Y] < self.n_stick_ymid):
            # Wait for A to be pressed.
            pass

        self.n_stick_ymin = self.state['nunchuk']['stick'][cwiid.Y]
        logging.debug('n_stick_ymin: {0}'.format(self.n_stick_ymin))
        self.led = 4
        time.sleep(1)

        print 'Push the nunchuk stick fully left, then press A.'
        while not (self.state['buttons'] & cwiid.BTN_A and self.state['nunchuk']['stick'][cwiid.X] < self.n_stick_xmid):
            # Wait for A to be pressed.
            pass

        self.n_stick_xmin = self.state['nunchuk']['stick'][cwiid.X]
        logging.debug('n_stick_xmin: {0}'.format(self.n_stick_xmin))
        self.led = 15

        # Save new settings to config
        if not self.config.has_section(self.mac_address):
            self.config.add_section(self.mac_address)
        self.config.set(self.mac_address,
                        'nunchuck stick xmid', self.n_stick_xmid)
        self.config.set(self.mac_address,
                        'nunchuck stick ymid', self.n_stick_ymid)
        self.config.set(self.mac_address, 'nunchuk stick xmax',
                        self.n_stick_xmax)
        self.config.set(self.mac_address, 'nunchuk stick ymax',
                        self.n_stick_ymax)
        self.config.set(self.mac_address,
                        'nunchuck stick xmin', self.n_stick_xmin)
        self.config.set(self.mac_address,
                        'nunchuck stick ymin', self.n_stick_ymin)
        with open(config_location, 'wb') as configfile:
            self.config.write(configfile)

        self.led = 0
        time.sleep(1)
        self.led = 15
        time.sleep(1)
        self.led = 0

    def n_stick_x_percent(self):
        left = 0.0
        right = 0.0
        try:
            stick_x = self.state['nunchuk']['stick'][cwiid.X]
        except KeyError:
            logging.error('No nunchuk attached.')
            raise
        if stick_x > self.n_stick_xmid:
            right = 100 * float(stick_x - self.n_stick_xmid) / \
                float(self.n_stick_xmax - self.n_stick_xmid)
        elif stick_x < self.n_stick_xmid:
            left = 100 - (100 * float(stick_x - self.n_stick_xmin) /
                          float(self.n_stick_xmid - self.n_stick_xmin))
        return (left, right)

    def n_stick_y_percent(self):
        up = 0.0
        down = 0.0
        try:
            stick_y = self.state['nunchuk']['stick'][cwiid.Y]
        except KeyError:
            logging.error('No nunchuk attached.')
            raise
        if stick_y > self.n_stick_ymid:
            up = 100 * float(stick_y - self.n_stick_ymid) / \
                float(self.n_stick_ymax - self.n_stick_ymid)
        elif stick_y < self.n_stick_ymid:
            down = 100 - (100 * float(stick_y - self.n_stick_ymin) /
                          float(self.n_stick_ymid - self.n_stick_ymin))
        return (up, down)

    @property
    def battery_percentage(self):
        return self.state['battery'] * 100 / cwiid.BATTERY_MAX

    @staticmethod
    def get_wiimote_mac_address():
        devices = bluetooth.discover_devices(lookup_names=True)
        for addr, name in devices:
            if name.startswith('Nintendo RVL-CNT-01'):
                logging.debug('Found Wiimote MAC address: {0}'.format(addr))
                return addr

    def get_config(self):
        logging.debug('Using config: {0}'.format(config_location))
        self.config = ConfigParser.ConfigParser()
        self.config.read(config_location)


def test():
    logging.basicConfig(level=logging.DEBUG)
    print 'Press 1 and 2 on the Wiimote at the same time to connect.'
    wiimote = None
    connect_attempt = 0
    while wiimote is None:
        connect_attempt += 1
        try:
            wiimote = WiimotePi()
        except RuntimeError:
            logging.info(
                'Trying to connect... Attempt {0:d}'.format(connect_attempt))

    pressed_buttons = []
    while True:
        del pressed_buttons[:]
        battery = wiimote.state['battery'] * 100 / cwiid.BATTERY_MAX
        logging.info('Battery: {0}'.format(battery))

        buttons = wiimote.state['buttons']
        if buttons & cwiid.BTN_HOME:
            logging.info('Home button pressed. Exiting.')
            wiimote.rumble = 1
            time.sleep(0.5)
            wiimote.rumble = 0
            wiimote.led = 0
            sys.exit()

        if buttons & cwiid.BTN_A:
            pressed_buttons.append('A')
        if buttons & cwiid.BTN_B:
            pressed_buttons.append('B')
        if buttons & cwiid.BTN_1:
            pressed_buttons.append('1')
        if buttons & cwiid.BTN_2:
            pressed_buttons.append('2')
        if buttons & cwiid.BTN_MINUS:
            pressed_buttons.append('-')
        if buttons & cwiid.BTN_PLUS:
            pressed_buttons.append('+')
        if buttons & cwiid.BTN_UP:
            pressed_buttons.append('up')
        if buttons & cwiid.BTN_DOWN:
            pressed_buttons.append('down')
        if buttons & cwiid.BTN_LEFT:
            pressed_buttons.append('left')
        if buttons & cwiid.BTN_RIGHT:
            pressed_buttons.append('right')

        if wiimote.state.has_key('nunchuk'):
            logging.info('Stick: {0}, {1}'.format(
                wiimote.n_stick_x_percent(), wiimote.n_stick_y_percent()))
            buttons_n = wiimote.state['nunchuk']['buttons']
            if buttons_n in (1, 3):
                pressed_buttons.append('z')
            if buttons_n > 1:
                pressed_buttons.append('c')

        logging.info('Buttons pressed: {0}.'.format(','.join(pressed_buttons)))

        # Currently unused sensors.

        # Accelerometer main X. Changes if mote is turned or flicked left or right.
        # Flat or upside down = 120, if turned: 90 degrees cc = 95, 90 degrees c = 145.
        # wiimote.state['acc'][cwiid.X]

        # Accelerometer main Y. Changes when pointed or flicked up or down.
        # Flat = 120, IR pointing up = 95, IR pointing down = 145.
        # wiimote.state['acc'][cwiid.Y]

        # Accelerometer main Z. Changes with rotation, or when pulled back or flicked up/down.
        # Flat = 145, 90 degrees cc or c, or 90 degrees up or down = 120, upside down = 95.
        # wiimote.state['acc'][cwiid.Z]

        # Accelerometer nunchuk X. 70 if tilted 90 degrees left, 175 if tilted 90 degrees right.
        # wiimote.state['nunchuk']['acc'][cwiid.X]

        # Accelerometer nunchuk Y. 70 if tilted 90 degrees down (buttons down), 175 if tilted 90 degrees up (buttons up).
        # wiimote.state['nunchuk']['acc'][cwiid.Y]

        # Accelerometer nunchuk Z. Nobody is quite sure about this one...
        # wiimote.state['nunchuk']['acc'][cwiid.Z]

if __name__ == '__main__':
    try:
        test()
    except KeyboardInterrupt:
        logging.info('End.')
