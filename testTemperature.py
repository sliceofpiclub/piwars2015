import RPi.GPIO as GPIO
import time
import threading

from Temperature import temperature
#
# Set a variable to the Pin number we what to use
# one for trigger output and one for echo input 
#

TRIG = 16  # GPIO 23
ECHO = 18  # GPIO 24

tempReading = 0

temperatureT = temperature.TemperatureThread('28-021500ed81ff')
temperatureT.start()
xLock = threading.Lock()

try:
    while True:
        xLock.acquire() 
        print temperatureT.TemperatureReading(),
        xLock.release()
        time.sleep(1)
except:
    temperatureT.stop()
 

