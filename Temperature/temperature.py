"""Temperature reading module.

Classes:
    TemperatureThread: Separate thread to read the temperature in the background.

Methods:
    readTemperature: Return current temperature reading in Centigrade and Fahrenheit.
"""

# ==========================================
# read the temperature and store in global
# variable.  This uses 1-wire temp reading
# ==========================================
#
# POWER -----------+------------- 3.3V [1]
#                  |
#                 470K
#                  |
# SIGNAL-----------+------------- GPIO 4 [7]
#
#
# GND --------------------------- GND [9 or 6]

__author__ = "Martin Eyre (Slice of Pi Club)"

# Standard library imports
import threading
import time


def readTemperature(deviceID):
    """Read temperature from 1-wire temp reading.

    Args:
        deviceID: ID of the temperature sensor.

    Returns:
        Temperature in Centigrade and Fahrenheit.
        -999, -999 if error raised in calculations.
    """
    try:
        tempC = -999
        tempF = -999

        f = open('/sys/bus/w1/devices/' + deviceID + '/w1_slave', 'r')
        line = f.readline()
        crc = line.strip()[-3:]

        if crc == 'YES':
            line = f.readline()
            tempPos = line.index('=')
            tempString = line[tempPos + 1:]
            tempC = float(tempString) / 1000.0
            tempF = tempC * 9.0 / 5.0 + 32.0

            f.close()

        return tempC, tempF
    except:
        return -999, -999


class TemperatureThread(threading.Thread):
    """Separate thread to read the temperature in the background."""

    def __init__(self, deviceID):
        """Initialise TemperatureThread class.

        Args:
            deviceID: ID of the temperature sensor.
        """
        threading.Thread.__init__(self)
        self.temperatureID = deviceID
        self.temperatureReading = -999
        self.finish = 0
        print "initialising temperature"
        time.sleep(2)

    def run(self):
        """Starts monitoring temperature."""
        print "Temperature Thread Started"
        while self.finish == 0:
            tempC, tempF = readTemperature(self.temperatureID)
            if tempC > 0:
                self.temperatureReading = tempC

            time.sleep(5)

    def stop(self):
        """Stops monitoring temperature."""
        print "Temperature Thread Stopping"
        self.finish = 1

    def TemperatureReading(self):
        """Takes a reading.

        Returns:
            Current temperature.
        """
        return self.temperatureReading
