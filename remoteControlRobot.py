"""Main file for remote control functionality.

Run using: sudo python remoteControlRobot.py
Press Ctrl-C to stop
"""
#!/usr/bin/env python
#
# Based on picocon example code.
#
# Up and down will make the robot go forward or backward.
# Left and right will make the robot spin left and right.
# Space = Stop
#
# Speed can be increased and reduced with Q and A.
#
# This makes the robot easy to operate, but can make erratic movements
# when high speeds are reached and switching between left and right
# or forward and backward.
#
# Added option to use Wiimote (WIP).
# ======================================================================

__author__ = "Martin Eyre & Claire Gurman (Slice of Pi Club)"

# Standard library imports
from datetime import datetime
import logging
import sys
import time

# Third party imports
import cwiid

# Local application/library specific imports
from control import keyboard, wiimote
from motorcontrol import picocon as move

# Logging options
DEBUG = False

# Control options
enable_wiimote = True
CONNECT_TIME_LIMIT = 300
DEADZONE = 10

# Movement constants
STOP = 0
FORWARD = 1
BACKWARD = 2
SPINLEFT = 3
SPINRIGHT = 4

# this gets set to 1 when an action key is pressed
action = 0
# this is the current action to perform
perform_action = STOP
# these are the left and right wheel speeds of the robot
speed = 30

# Set log level
if DEBUG:
    log_level = logging.DEBUG
else:
    log_level = logging.WARNING
logging.basicConfig(format='%(asctime)s: %(levelname)s: %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p', level=log_level)

# initialise the picocon motor controller
move.init()

try:
    if enable_wiimote:
        print 'Press 1 and 2 on the Wiimote at the same time to connect.'
        controller = None
        connect_attempt = 0
        connect_timer = datetime.now()
        wiimote_address = None
        while wiimote_address is None:
            connecting_time = datetime.now() - connect_timer
            if connecting_time.seconds > CONNECT_TIME_LIMIT:
                print '{0:d} seconds spent trying to connect. Giving up.'.format(CONNECT_TIME_LIMIT)
                sys.exit()
            wiimote_address = wiimote.WiimotePi.get_wiimote_mac_address()
        logging.debug('Using Wiimote address: {0}.'.format(wiimote_address))
        while controller is None:
            connecting_time = datetime.now() - connect_timer
            if connecting_time.seconds > CONNECT_TIME_LIMIT:
                print '{0:d} seconds spent trying to connect. Giving up.'.format(CONNECT_TIME_LIMIT)
                sys.exit()
            logging.debug('Attempting connection for {0} seconds.'.format(
                connecting_time.seconds))
            connect_attempt += 1
            try:
                controller = wiimote.WiimotePi(wiimote_address)
            except RuntimeError:
                logging.info(
                    'Trying to connect... Attempt {0:d}'.format(connect_attempt))
            except KeyboardInterrupt:
                print 'Wiimote connecting aborted. Exiting.'
                sys.exit()

        # Indicate connection
        if controller.state.has_key('nunchuk'):
            controller.get_settings()
        controller.rumble = 1
        time.sleep(0.5)
        controller.rumble = 0
        logging.info('Battery: {0}%'.format(controller.battery_percentage))
        print 'Off we go...!'

    # ---------------------------------
    # Main loop
    # ---------------------------------
    while True:
        if enable_wiimote and not controller.state.has_key('nunchuk'):
            print 'No nunchuk detected. Wiimote disabled, please use keyboard.'
            enable_wiimote = False

        if enable_wiimote and controller.state.has_key('nunchuk'):
            input_x = controller.n_stick_x_percent()
            input_y = controller.n_stick_y_percent()
            buttons = controller.state['buttons']

            logging.debug('{0} - {1}'.format(input_x, input_y))

            if all(yvalue <= DEADZONE for yvalue in input_y):
                if input_x[0] > DEADZONE:
                    move.spinLeft(input_x[0])
                elif input_x[1] > DEADZONE:
                    move.spinRight(input_x[1])
                else:
                    move.stop()
            elif input_y[0] > DEADZONE:
                if input_x[0] > DEADZONE:
                    move.turnForward(100 - input_x[0], input_y[0])
                elif input_x[1] > DEADZONE:
                    move.turnForward(input_y[0], 100 - input_x[1])
                else:
                    move.forward(input_y[0])
            elif input_y[1] > DEADZONE:
                if input_x[0] > DEADZONE:
                    move.turnReverse(100 - input_x[0], input_y[1])
                elif input_x[1] > DEADZONE:
                    move.turnReverse(input_y[1], 100 - input_x[1])
                else:
                    move.reverse(input_y[1])
            else:
                move.stop()

            if buttons & cwiid.BTN_HOME:
                move.stop()
                controller.calibrate()

        else:
            action = 0
            keyp = keyboard.readkey()

            # set the correct action
            if ord(keyp) == 16:
                action = 1
                perform_action = FORWARD
            elif ord(keyp) == 17:
                action = 1
                perform_action = BACKWARD
            elif ord(keyp) == 18:
                action = 1
                perform_action = SPINRIGHT
            elif ord(keyp) == 19:
                action = 1
                perform_action = SPINLEFT
            elif keyp == 'z':
                if speed > 10:
                    speed = speed - 10
                action = 1
                # no need to set what action to perform just do the last one
            elif keyp == 'a':
                if speed < 100:
                    speed = speed + 10
                action = 1
                # no need to set what action to perform just do the last one
            elif keyp == ' ':
                action = 1
                perform_action = STOP
            elif ord(keyp) == 3:
                break

            # if an action is required
            # perform the required action
            if action == 1:
                action = 0
                if perform_action == STOP:
                    move.stop()
                    print 'Stop'
                elif perform_action == FORWARD:
                    move.forward(speed)
                    print 'forward', speed
                elif perform_action == BACKWARD:
                    move.reverse(speed)
                    print 'reverse', speed
                elif perform_action == SPINLEFT:
                    move.spinLeft(speed)
                    print 'spin left', speed
                elif perform_action == SPINRIGHT:
                    move.spinRight(speed)
                    print 'spin right', speed

except KeyboardInterrupt:
    print "Interrupted. Stopping."

finally:
    move.cleanup()
