"""Main file for the proximity sensing functionality.

Run using: sudo python proximity.py
Press Ctrl-C to stop
"""
#!/usr/bin/env python
#
# Moves: Forward until close to wall
#        when distance < 23 slow down
#        when distance < 18 slow down further
#        when distance < 12 Stop
# ======================================================================

__author__ = "Martin Eyre (Slice of Pi Club)"

# Standard library imports
import time

# Local application/library specific imports
from control import keyboard
from motorcontrol import picocon as move
from Temperature import temperature
from ultrasonic import ultrasonic
from LED import LED

# Movement constants
PERCENT_LEFT = 100    # adjust power of motors to match their real speed
PERCENT_RIGHT = 95
SPEED_SLOW = 10
SPEED_SLOWER = 5
SPEED_SLOWEST = 1

# Distance constants
SLOW_DISTANCE = 20.0
SLOWER_DISTANCE = 15.0
STOP_DISTANCE = 5.3
NUDGE_DISTANCE = 8.0
NUDGE_COUNTER = 10
NORMAL_COUNTER = 3

# Action constants
ACTION_STOP = 0
ACTION_FORWARD = 1
ACTION_SLOW = 2
ACTION_SLOWER = 3
ACTION_NUDGE = 4

# Debug constants
DEBUG = 1

# initialise the libraries
move.init()
ultrasonic.init()
LED.init()

try:
    # read the ambient temperature
    tempC, tempF = temperature.readTemperature('28-021500ed81ff')

    # ---------------------------------
    # Main loop
    # ---------------------------------
    stopped = False
    while not stopped:
        #LED.LEDon()
        print "Waiting for key press..."

        # --- GET KEYBOARD INPUT
        keyboard.readkey()

        action = ACTION_FORWARD
        speed = 30
        reset = False
        avg_counter = NORMAL_COUNTER

        print "Starting................."

        if DEBUG:
            print "SLOW_DISTANCE", SLOW_DISTANCE
            print "SLOWER_DISTANCE", SLOWER_DISTANCE
            print "STOP_DISTANCE", STOP_DISTANCE

        startTime = time.time()
        #LED.LEDoff()

        while not reset:

            #--- increase speed until max speed
            if action == ACTION_FORWARD:
                if speed < 100:
                    speed += 10
                    move.forwardPercent(speed, PERCENT_LEFT, PERCENT_RIGHT)
                    if DEBUG:
                        print "increase speed ", speed

            # --- Get the distance from the wall
            distance, distanceTemp = ultrasonic.AverageDistance(tempC, avg_counter)
            if DEBUG:
                print distanceTemp

            # --- check if near to wall
            if distanceTemp < SLOW_DISTANCE and action == ACTION_FORWARD:
                action = ACTION_SLOW
                speed = SPEED_SLOW
                move.forwardPercent(speed, PERCENT_LEFT, PERCENT_RIGHT)
                if DEBUG:
                    print "Slow"

            # --- check if near to wall
            if distanceTemp < SLOWER_DISTANCE and action == ACTION_SLOW:
                action = ACTION_SLOWER
                speed = SPEED_SLOWER
                move.forward(speed)
                if DEBUG:
                    print "Slower"

            if distanceTemp < NUDGE_DISTANCE and action == ACTION_SLOWER:
                move.stop()
                action = ACTION_NUDGE
                speed = SPEED_SLOWEST
                avg_counter = NUDGE_COUNTER
                if DEBUG:
                    print "Nudge!!"

            # if in nudge mode go forward a bit!
            if action == ACTION_NUDGE:
                move.forward(speed)
                time.sleep(0.5)
                move.stop()

            # --- check if very close to wall
            if distanceTemp < STOP_DISTANCE:
                move.stop()
                endTime = time.time()
                if DEBUG:
                    print "Time taken", endTime - startTime
                    print "STOP !!!"
                reset = True

except KeyboardInterrupt:
    print "Interrupted. Stopping."

finally:
    move.cleanup()
    print 'Finish'
