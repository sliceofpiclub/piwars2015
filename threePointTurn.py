"""Main file for the three point turn functionality.

Run using: sudo python threePointTurn.py
Press Ctrl-C to stop
"""
#!/usr/bin/env python
#
# Moves forward until a line is detected,
# then turns 90 degrees to the left,
# then goes forward until a line is detected
# then reverses backward until a line is detected,
# then goes forward to the mid point
# then turns 90 degrees left
# then goes forward looking for box
# stops after the line is detected
# ======================================================================

__author__ = "Martin Eyre (Slice of Pi Club)"

# Standard library imports
from subprocess import call
import time

# Third party imports
import cv2
import numpy as np

# Local application/library specific imports
from motorcontrol import picocon as move

# Movement constants
CYCLES_PER_CM = 600
MAXPOWER = 100
REVERSE = 3
REVERSE_TIMED = 5
SPEED = 100
STOP = 0
STRAIGHT = 1
STRAIGHT_TIMED = 4
STRAIGHT_TIMED_CYCLES = 30
TURN_RIGHT_90 = 2

ACTION_STEPS = [STOP, STRAIGHT_TIMED, REVERSE_TIMED, STOP, TURN_RIGHT_90, STOP, STRAIGHT_TIMED,
                REVERSE_TIMED, STOP, REVERSE_TIMED, STRAIGHT_TIMED, TURN_RIGHT_90, STRAIGHT_TIMED, REVERSE_TIMED, STOP]
ACTION_CM = [0, 125, 1, 50, 17, 50, 22, 1, 50, 48, 25, 15, 123, 1, 0]

# Camera constants
CAM_FRAMERATE = 30
CAM_HEIGHT = 144
CAM_WIDTH = 256
CENTRE = 128

# Detection constants
DETECTION_DELAY_CYCLES = 90
TURN_CYCLES = 12000
Y_ROW = 1

# Debug constants
DEBUG = 1
SHOWIMAGE = 0

# Global variables
actionStep = 0
sharpTurnCycleCount = 0


def initCamera():
    """Initialise camera"""
    global camera
    print "camera initialise"

    # Load camera driver
    call('sudo modprobe bcm2835-v4l2', shell=True)

    camera = cv2.VideoCapture(0)
    if not camera:
        print "camera failed to initialise"

    if not camera.isOpened():
        print "opening Camera"
        camera.open(0)
    camera.set(3, CAM_WIDTH)
    camera.set(4, CAM_HEIGHT)
    camera.set(5, CAM_FRAMERATE)
    time.sleep(2)

    ret, raw_capture = camera.read()

    raw_capture = []


def closeCamera():
    """Close camera"""
    camera.release()
    cv2.destroyAllWindows()
    print "camera closed"


def detectLine():
    """Detect line using camera"""
    # ---------------------------------
    # Read a frame from the video camera.
    # Scan a line and check for a line to fill the frame from right to left.
    #
    # If something goes wrong then the last known good values are used.
    # ---------------------------------
    global camera

    lineDetected = 0
    ret, raw_capture = camera.read()

    # check a frame was captured correctly
    if ret:
        # convert to grey scales
        y_grey = cv2.cvtColor(raw_capture, cv2.COLOR_BGR2GRAY)
        # Gaussian Blur helps remove any smaller discrepancies on the page
        y_filtered = cv2.GaussianBlur(y_grey, (5, 5), 0)

        #
        retVal, y_binary = cv2.threshold(
            y_filtered, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

        # if the threshold was filtered correctly
        if retVal:
            if SHOWIMAGE:
                cv2.imshow("Frame", y_binary)
                cv2.waitKey(10)

            # extract one row from the frame
            # the line will be full of 1's and 0's, 1 represents the line
            #
            # 000000000000000111111111111100000000000000000000
            #                > line here <
            #
            y_line = y_binary[Y_ROW, :]

            # locate the start and end of the line (first max value)
            # locate the end (last max value)
            start_line_position = y_line.argmax()
            end_line_position = len(y_line) - 1 - y_line[::-1].argmax()

            # check if full line
            if start_line_position == 0 and end_line_position == 255:
                lineDetected = 1

    # if DEBUG:
    # print "start_line_position ",start_line_position ," end_line_position
    # ",end_line_position, "lineDetected",lineDetected

    return lineDetected

# initialise the libraries
move.init()

# main loop forever until stop
try:
    # initCamera()
    actionNumber = 0
    detection = 0
    detectionDelay = 0
    turnCycle = 0
    timerCycle = 0

    startTime = time.time()
    counter = 0
    # repeat until all steps are complete
    while actionNumber < len(ACTION_STEPS) - 1:
        counter = counter + 1
        changeAction = 0

        # get the current action
        currentAction = ACTION_STEPS[actionNumber]
        currentCycles = ACTION_CM[actionNumber] * CYCLES_PER_CM

        # look to see if we can see a line
        # lineDetected = detectLine()

        if currentAction == STOP:
            move.stop()
            # when timer expires move to next action
            timerCycle = timerCycle + 1
            if timerCycle > currentCycles:
                timerCycle = 0
                changeAction = 1
        elif currentAction == STRAIGHT:
            move.forward(MAXPOWER)
            # if line detected and we are in detection mode
            if lineDetected and detection == 1:
                changeAction = 1
        elif currentAction == TURN_RIGHT_90:
            move.spinLeft(MAXPOWER)
            # turnCycle = turnCycle + 1
            # if turnCycle > TURN_CYCLES:
            #    turnCycle = 0
            #    changeAction=1
            timerCycle = timerCycle + 1
            if timerCycle > currentCycles:
                timerCycle = 0
                changeAction = 1
        elif currentAction == REVERSE:
            move.reverse(MAXPOWER)
            # if line detected and we are in detection mode
            if lineDetected and detection == 1:
                changeAction=1
        elif currentAction == STRAIGHT_TIMED:
            move.forward(MAXPOWER)
            # when timer expires move to next action
            timerCycle = timerCycle + 1
            if timerCycle > currentCycles:
                timerCycle = 0
                changeAction=1
        elif currentAction == REVERSE_TIMED:
            move.reverse(MAXPOWER)
            timerCycle = timerCycle + 1
            if timerCycle > currentCycles:
                timerCycle = 0
                changeAction = 1
        # if its time to change the action
        if changeAction==1:
            print "currentCycles",currentCycles,"timerCycle",timerCycle

            # reset the change flag
            changeAction=0

            # set detection off and start the delay timer
            detection = 0
            detectionDelay = 1
            print "detection OFF!"

            # move the the next action in the list
            actionNumber = actionNumber + 1

            if DEBUG:
                print "actionNumber",actionNumber
                if ACTION_STEPS[actionNumber] == STOP:
                    print "STOP"
                elif ACTION_STEPS[actionNumber] == STRAIGHT:
                    print "STRAIGHT"
                elif ACTION_STEPS[actionNumber] == TURN_RIGHT_90:
                    print "TURN"
                elif ACTION_STEPS[actionNumber] == REVERSE:
                    print "REVERSE"
                elif ACTION_STEPS[actionNumber] == STRAIGHT_TIMED:
                    print "TIMED STRAIGHT"
                elif ACTION_STEPS[actionNumber] == REVERSE_TIMED:
                    print "TIMED REVERSE"

        # if the detection delay is set
        if detectionDelay > 0:
            # increase the delay
            detectionDelay = detectionDelay + 1
            # when the detection delay has reached the correct number of cycles
            if detectionDelay > DETECTION_DELAY_CYCLES:
                # reset the delay and set to start detection
                detectionDelay = 0
                detection = 1
                print "detection on !"

except KeyboardInterrupt:
    print "Interrupted. Stopping."

finally:
    move.cleanup()
    #closeCamera()
    print 'Finish'
