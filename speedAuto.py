"""Main file for the straight line speed test.

Run using: sudo python speedAuto.py
Press Ctrl-C to stop
"""
#!/usr/bin/env python
#
# Straight line speed test - with ultrasonic correction.
#
# The robot will go forward at the power defined.  Reading the distance
# on the right, if it falls below thresh hold it will adjust.
# ======================================================================

__author__ = "Martin Eyre (Slice of Pi Club)"

# Local application/library specific imports
from control import keyboard
from motorcontrol import picocon as move
from ultrasonic import ultrasonic

# these are global constants
DISTANCE = 20
LEFT_PERCENTAGE = 100
RIGHT_PERCENTAGE = 90
DRIFT_CYCLES = 7
DRIFT_PERCENTAGE = 50
SPEED = 100
DEBUG = 1

# initialise variables
drift_cycle = 0
drift_percentage = 0

# initialise the picocon motor controller and ultrasonics
move.init()
ultrasonic.init()
print "Press any key to start ...."
keyboard.readkey()
print "Go, Go, Go !!!"

try:
    # ---------------------------------
    # Main loop
    # ---------------------------------
    while True:
        move.forwardPercent(SPEED, LEFT_PERCENTAGE -
                            drift_percentage, RIGHT_PERCENTAGE)

        # read the distance - temperature is not used here as it does not need
        # to be accurate
        distance, distanceTemp = ultrasonic.AverageDistance(-999, 3)

        # if the distance is below threshold and not already in a correction
        # cycle
        if distance < DISTANCE and drift_cycle == 0:
            drift_cycle = 1
            drift_percentage = DRIFT_PERCENTAGE

        # if in drift mode, do this for so many cycles
        if drift_cycle > 0:
            if DEBUG:
                print "Drift correction", drift_cycle
            drift_cycle = drift_cycle + 1
            if drift_cycle > DRIFT_CYCLES:
                drift_cycle = 0
                drift_percentage = 0
                if DEBUG:
                    print "-------------------------"

except KeyboardInterrupt:
    print "Interrupted. Stopping."

finally:
    move.cleanup()
