# README #

This README would normally document whatever steps are necessary to get the application up and running.

## [Pi Wars](http://piwars.org/) ##

More detailed information about the competition and challenges can be found at [http://piwars.org/](http://piwars.org/).

## Pi Preparation ##

To utilise the Wiimote, bluetooth drivers...
```
sudo apt-get update
sudo apt-get install python-pip python-dev ipython

sudo apt-get install bluetooth libbluetooth-dev
sudo pip install pybluez
```
..and then the cwiid library will need to be installed...
```
sudo apt-get install python-cwiid
```
[Source of info](https://www.raspberrypi.org/learning/robo-butler/bluetooth-setup/)