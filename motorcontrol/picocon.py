#!/usr/bin/python
#
# Python Module to externalise Picocon Motor Driver hardware
#
# Created by Gareth Davies, Sep 2013
# Copyright 4tronix
#
# This code is in the public domain and may be freely copied and used
# No warranty is provided or implied
#
#======================================================================


#======================================================================
# General Functions
#
# init(). Initialises GPIO pins, switches motors off, etc
# cleanup(). Sets all motors off and sets GPIO to standard values

# Import all necessary libraries
import RPi.GPIO as GPIO
#, sys, threading, time, os

# Choose pinout
AGOBO = True

# Set limits
SPEED_MAX = 100
SPEED_MIN = 0

if AGOBO:
    # Pins 24, 26 Left Motor
    # Pins 19, 21 Right Motor
    R1 = 19
    R2 = 21
    L1 = 26
    L2 = 24
else:
    # Pins 24, 26 Right Motor
    # Pins 19, 21 Left Motor
    # Because we like to be different...
    R1 = 24
    R2 = 26
    L1 = 19
    L2 = 21

#======================================================================
# General Functions
#
# init(). Initialises GPIO pins, switches motors and LEDs Off, etc


def init():
    global p, q, a, b

    # use physical pin numbering
    GPIO.setmode(GPIO.BOARD)

    # use pwm on motor outputs so motors can be controlled
    GPIO.setup(L1, GPIO.OUT)
    p = GPIO.PWM(L1, 20)
    p.start(0)

    GPIO.setup(L2, GPIO.OUT)
    q = GPIO.PWM(L2, 20)
    q.start(0)

    GPIO.setup(R1, GPIO.OUT)
    a = GPIO.PWM(R1, 20)
    a.start(0)

    GPIO.setup(R2, GPIO.OUT)
    b = GPIO.PWM(R2, 20)
    b.start(0)

# cleanup(). Sets all motors off and sets GPIO to standard values


def cleanup():
    stop()
    GPIO.cleanup()

# End of General Functions
#======================================================================


#======================================================================
# Motor Functions
#
# stop(): Stops both motors
def stop():
    p.ChangeDutyCycle(0)
    q.ChangeDutyCycle(0)
    a.ChangeDutyCycle(0)
    b.ChangeDutyCycle(0)

# forward(speed): Sets both motors to move forward at speed. 0 <= speed <= 100


def forward(speed):
    speed = min(speed, SPEED_MAX)
    speed = max(speed, SPEED_MIN)
    p.ChangeDutyCycle(speed)
    q.ChangeDutyCycle(0)
    a.ChangeDutyCycle(speed)
    b.ChangeDutyCycle(0)
    p.ChangeFrequency(speed + 5)
    a.ChangeFrequency(speed + 5)

# reverse(speed): Sets both motors to reverse at speed. 0 <= speed <= 100


def reverse(speed):
    speed = min(speed, SPEED_MAX)
    speed = max(speed, SPEED_MIN)
    p.ChangeDutyCycle(0)
    q.ChangeDutyCycle(speed)
    a.ChangeDutyCycle(0)
    b.ChangeDutyCycle(speed)
    q.ChangeFrequency(speed + 5)
    b.ChangeFrequency(speed + 5)

# spinLeft(speed): Sets motors to turn opposite directions at speed. 0 <=
# speed <= 100


def spinLeft(speed):
    speed = min(speed, SPEED_MAX)
    speed = max(speed, SPEED_MIN)
    p.ChangeDutyCycle(0)
    q.ChangeDutyCycle(speed)
    a.ChangeDutyCycle(speed)
    b.ChangeDutyCycle(0)
    q.ChangeFrequency(speed + 5)
    a.ChangeFrequency(speed + 5)

# spinRight(speed): Sets motors to turn opposite directions at speed. 0 <=
# speed <= 100


def spinRight(speed):
    speed = min(speed, SPEED_MAX)
    speed = max(speed, SPEED_MIN)
    p.ChangeDutyCycle(speed)
    q.ChangeDutyCycle(0)
    a.ChangeDutyCycle(0)
    b.ChangeDutyCycle(speed)
    p.ChangeFrequency(speed + 5)
    b.ChangeFrequency(speed + 5)

# turnForward(leftSpeed, rightSpeed): Moves forwards in an arc by setting
# different speeds. 0 <= leftSpeed,rightSpeed <= 100


def turnForward(leftSpeed, rightSpeed):
    leftSpeed = min(leftSpeed, SPEED_MAX)
    leftSpeed = max(leftSpeed, SPEED_MIN)
    rightSpeed = min(rightSpeed, SPEED_MAX)
    rightSpeed = max(rightSpeed, SPEED_MIN)
    p.ChangeDutyCycle(leftSpeed)
    q.ChangeDutyCycle(0)
    a.ChangeDutyCycle(rightSpeed)
    b.ChangeDutyCycle(0)
    p.ChangeFrequency(leftSpeed + 5)
    a.ChangeFrequency(rightSpeed + 5)

# turnReverse(leftSpeed, rightSpeed): Moves backwards in an arc by setting
# different speeds. 0 <= leftSpeed,rightSpeed <= 100


def turnReverse(leftSpeed, rightSpeed):
    leftSpeed = min(leftSpeed, SPEED_MAX)
    leftSpeed = max(leftSpeed, SPEED_MIN)
    rightSpeed = min(rightSpeed, SPEED_MAX)
    rightSpeed = max(rightSpeed, SPEED_MIN)
    p.ChangeDutyCycle(0)
    q.ChangeDutyCycle(leftSpeed)
    a.ChangeDutyCycle(0)
    b.ChangeDutyCycle(rightSpeed)
    q.ChangeFrequency(leftSpeed + 5)
    b.ChangeFrequency(rightSpeed + 5)

# ForwardPercent(speed, percentLeft, percentRight): Moves


def forwardReverse(leftSpeed, rightSpeed):
    leftSpeed = min(leftSpeed, SPEED_MAX)
    leftSpeed = max(leftSpeed, SPEED_MIN)
    rightSpeed = min(rightSpeed, SPEED_MAX)
    rightSpeed = max(rightSpeed, SPEED_MIN)
    if leftSpeed > 0:
        p.ChangeDutyCycle(leftSpeed)
        q.ChangeDutyCycle(0)
    else:
        leftSpeed = -leftSpeed
        p.ChangeDutyCycle(0)
        q.ChangeDutyCycle(leftSpeed)

    if rightSpeed > 0:
        a.ChangeDutyCycle(rightSpeed)
        b.ChangeDutyCycle(0)
    else:
        rightSpeed = -rightSpeed
        a.ChangeDutyCycle(0)
        b.ChangeDutyCycle(rightSpeed)

    p.ChangeFrequency(leftSpeed + 5)
    a.ChangeFrequency(rightSpeed + 5)


# ForwardPercent(speed, percentLeft, percentRight): Moves
def forwardPercent(speed, percentLeft, percentRight):
    speed = min(speed, SPEED_MAX)
    speed = max(speed, SPEED_MIN)
    leftSpeed = speed * percentLeft / 100
    rightSpeed = speed * percentRight / 100

    if leftSpeed > 0:
        p.ChangeDutyCycle(leftSpeed)
        q.ChangeDutyCycle(0)
    else:
        leftSpeed = -leftSpeed
        p.ChangeDutyCycle(0)
        q.ChangeDutyCycle(leftSpeed)

    if rightSpeed > 0:
        a.ChangeDutyCycle(rightSpeed)
        b.ChangeDutyCycle(0)
    else:
        rightSpeed = -rightSpeed
        a.ChangeDutyCycle(0)
        b.ChangeDutyCycle(rightSpeed)

    p.ChangeFrequency(leftSpeed + 5)
    a.ChangeFrequency(rightSpeed + 5)


# End of Motor Functions
#======================================================================
