# picocon Motor Test
# Moves: Forward, Reverse, turn Right, turn Left, Stop - then repeat
# Press Ctrl-C to stop
#
# Also demonstrates writing to the LEDs
#
# To check wiring is correct ensure the order of movement as above is correct
# Run using: sudo python motorTest.py


import picocon, time

#======================================================================
# Reading single character by forcing stdin to raw mode
import sys
import tty
import termios

def readchar():
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    if ch == '0x03':
        raise KeyboardInterrupt
    return ch

def readkey(getchar_fn=None):
    getchar = getchar_fn or readchar
    c1 = getchar()
    if ord(c1) != 0x1b:
        return c1
    c2 = getchar()
    if ord(c2) != 0x5b:
        return c1
    c3 = getchar()
    return chr(0x10 + ord(c3) - 65)  # 16=Up, 17=Down, 18=Right, 19=Left arrows

# End of single character reading
#======================================================================

speedLeft = 0
speedRight = 0
currentSpeedLeft = 0
currentSpeedRight = 0

maxSpeed =100

action = 0
turnLeft =0
turnRight=0
picocon.init()

# main loop
try:
    while True:
	if action == 1:
	    print 'Forward'		

	turnLeft = 0
	turnRight=0

        keyp = readkey()
	if keyp == 'q':
	    if speedLeft < maxSpeed:
	        speedLeft += 10
	elif keyp  == 'a':
	    if speedLeft > 0:
		speedLeft -= 10
	elif keyp == 'w':
	    if speedRight < maxSpeed:
		speedRight += 10
	elif keyp == 's':
	    if speedRight > 0:
		speedRight -= 10
	elif keyp == ' ':
	    picocon.stop();
	    speedRight = 0
	    speedLeft = 0
	elif ord(keyp) == 16:
	    if speedRight < maxSpeed:
		speedRight += 10
	    if speedLeft < maxSpeed:
		speedLeft += 10;
	elif ord(keyp) == 17:
	    if speedRight > 0:
		speedRight -=10
	    if speedLeft > 0:
		speedLeft -= 10
	elif ord(keyp) == 18:
	    turnLeft = 1
            turnRight = 0
	elif ord(keyp) == 19:
	    turnRight = 1
	    turnLeft = 0	 
	elif ord(keyp) == 3:
	    break;

	if speedRight <> currentSpeedRight or speedLeft <> currentSpeedLeft:
	    currentSpeedRight = speedRight
	    currentSpeedLeft = speedLeft
	if turnLeft == 1:
	    picocon.turnReverse(speedLeft,0)
	elif turnRight == 1:
	    picocon.turnReverse(0,speedRight)
	else:		
   	   picocon.turnReverse(speedLeft, speedRight)
      
	turnLeft = 0
	turnRight = 0

except KeyboardInterrupt:
    print

finally:
    picocon.cleanup()
    
