"""Ultrasonic distance reading module.

Methods:
    init: Initialise the ultrasonic sensor.
    measure: Takes a distance measurement.
    AverageDistance: Takes the average distance of three measurements.
"""

# ==========================================
# interface to a ultrasonic range sensor
# HC-SR04 1.99 Ebay
# ==========================================
# HC-SR04, 1k ohm Resistor, 2kOhm Resistor
#
# GND -------------+------------- GPIO GND [6]
#                  |
#                  2K
#                  |
# ECHO ----1K------+------------- GPIO 24 [18]
#
# TRIG -------------------------- GPIO 23 [16]
#
# Vcc --------------------------- GPIO 5V [2]

__author__ = "Martin Eyre (Slice of Pi Club)"

# Standard library imports
import RPi.GPIO as GPIO
import time

# Set a variable to the Pin number we what to use
# one for trigger output and one for echo input

TRIG = 16  # GPIO 23
ECHO = 18  # GPIO 24


def init():
    """Initialise ultrasonic sensor."""
    print 'initialising ultrasonic'

    # Use BCM e.g. by name noT BOARD by pin number
    GPIO.setmode(GPIO.BOARD)

    # Set warnings off to suppress re-initialisation messages
    GPIO.setwarnings(False)

    # Initialise the GPIO Ports to OUTPUT
    print "Initialising GPIO for Measuring Distance"
    GPIO.setup(TRIG, GPIO.OUT, initial=GPIO.LOW)
    GPIO.setup(ECHO, GPIO.IN,  pull_up_down=GPIO.PUD_UP)

    # Stabilise sensor
    print "Wait 2 seconds for sensor stabilisation"
    time.sleep(2)


def measure(temperature):
    """Take a distance measurement.

    Args:
        temperature: if a temperature is passed that is greater than 0 then
            it will be used when calculating the speed of sound.

    Returns:
        distance in cm, distanceTemp in degC
    """
    #--------ping the trigger high then low
    GPIO.output(TRIG, True)
    time.sleep(0.00001)
    GPIO.output(TRIG, False)

    #--------keep reading the time while echo is low
    start = time.time()
    while GPIO.input(ECHO) == 0:
        start = time.time()

    #--------keep reading the time while echo is high
    stop = time.time()
    while GPIO.input(ECHO) == 1:
        stop = time.time()

    #--------calc the distance
    duration = stop - start

    speedOfSound = 17500.0
    if temperature > 0.0:
        speedOfSound = (331.3 + (0.606 * temperature)) * 100.0 / 2.0

    distanceTemp = duration * speedOfSound
    distanceTemp = round(distanceTemp, 2)

    distance = duration * 17500.0
    distance = round(distance, 2)

    return distance, distanceTemp


def AverageDistance(temp, counter):
    """Takes the average of counter number of distance readings.

    Args:
        temp: if a temperature is passed that is greater than 0 then it will be used for a more accurate measurement.
        counter: number of readings to average.

    Returns:
        average distance in cm, temperature in degC
    """
    readings_temp = []
    readings = []
    while counter > 0:
        time.sleep(0.1)
        r, rt = measure(temp)
        readings.append(r)
        readings_temp.append(r)
        counter -= 1

    d2 = sum(readings) / float(len(readings))
    d2t = sum(readings_temp) / float(len(readings_temp))

    d2 = round(d2, 1)
    d2t = round(d2t, 1)

    return d2, d2t
