# LED GPIO library
#======================================================================
# 

import RPi.GPIO as GPIO
import time
import threading

GPIO_LED_PIN = 26

# ===============================================
# initialise the LED
# ===============================================
def init():
    print 'initalising LED'

    #
    # Use BCM e.g. by name noT BOARD by pin number
    #

    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(GPIO_LED_PIN, GPIO.OUT)

def flash(times, delay):
    counter = 0
    while counter < times:
        flashOnce(delay)
        counter = counter + 1

# flash LED once 
def flashOnce(delay):
    time.sleep(delay)
    LEDon()
    time.sleep(delay)
    LEDoff()



# turn LED on
def LEDon():
    GPIO.output(GPIO_LED_PIN, GPIO.HIGH)

# turn LED of
def LEDoff():
    GPIO.output(GPIO_LED_PIN, GPIO.LOW)

def finish():
    print "finsihed LED"
    GPIO.cleanup()


class LEDThread(threading.Thread):
    delay = 0
    finish = 0
   
    def __init__(self,delay):
        threading.Thread.__init__(self)
        self.finish = 0 
        self.delay = delay
        init()
	print "initialising LED Thread"

    def setDelay(self, delay):
        self.delay = delay

    def run(self):
        print "Temperature Thread Started"
        while self.finish == 0:
            if self.delay == 0:
                print "ON"
                LEDon()
                time.sleep(1)
            else: 
                flashOnce(self.delay)
           
    def stop(self):
        print "LED Thread Stopping"        
        self.finish = 1
        time.sleep(1+self.delay)
	LEDoff()

 
# End of LED libs
#======================================================================